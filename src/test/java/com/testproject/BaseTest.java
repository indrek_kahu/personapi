package com.testproject;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class BaseTest {

    protected String getResponseJson(String name) {
        ObjectMapper mapper = new ObjectMapper();
        File resourceFile = new File("src/test/resources/responses/" + name + ".json");
        Object jsonObject = null;

        try {
            jsonObject = mapper.readValue(resourceFile, Object.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return asJsonString(jsonObject);
    }

    protected String getResponseJson(String name, String value) {
        ObjectMapper mapper = new ObjectMapper();
        File resourceFile = new File("src/test/resources/responses/" + name + ".json");
        Object jsonObject = value;

        try {
            jsonObject = mapper.readValue(resourceFile, Object.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return asJsonString(jsonObject);
    }

    protected Object getRequestJson(String name, Class classToRead) {
        ObjectMapper mapper = new ObjectMapper();
        File resourceFile = new File("src/test/resources/requests/" + name + ".json");
        try {
            return mapper.readValue(resourceFile, classToRead);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected String asJsonString(final Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
