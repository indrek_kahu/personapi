package com.testproject;

import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.testproject.Utils.Constants;
import com.testproject.transformers.DynamicTransformer;
import org.junit.Rule;


import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class MockTestServer extends BaseTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().port(Constants.PORT).extensions(new DynamicTransformer()), false);

}
