package com.testproject.services;

import com.testproject.MockTestServer;
import com.testproject.Utils.Constants;
import com.testproject.Utils.Helper;
import com.testproject.model.requests.PersonRequest;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.testproject.Responses.doGet;
import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

public class PersonTest extends MockTestServer {

    @Test
    public void isPersonAvailable() {
        createValidPerson();
        assertEquals(doGet(Helper.linkBuilder(Constants.URL,Constants.PORT,"/api/person/personal-code/2134546"), PersonRequest.class).getStatusCode(), 200);
    }

    private Response getResponse() {
        return given().port(Constants.PORT).accept(ContentType.JSON).when().get(Helper.linkBuilder(Constants.URL,Constants.PORT,"/api/person/personal-code/2134546")).then().extract().response();
    }

    private void createValidPerson() {
        stubFor(get(urlPathMatching("/api/person/personal-code/[^/]"))
                .willReturn(aResponse().withTransformerParameter("fileName","person_response")
                        .withTransformers("dynamic-transformer")));
    }
}