package com.testproject;


import com.testproject.responses.ErrorResponse;

/**
 * Default Bravio response class
 */
public class ApiTestResponse {

    private int statusCode;
    private String message;
    private Object data;

    public ApiTestResponse(int statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
        this.data = new Object();
    }

    public ApiTestResponse(int statusCode, Object data) {
        this.statusCode = statusCode;

        try {
            this.message = ((ErrorResponse) data).getMessage();
        } catch (Exception e) {
            this.message = "OK";
        }

        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    public Object getData() {
        return data;
    }

}
