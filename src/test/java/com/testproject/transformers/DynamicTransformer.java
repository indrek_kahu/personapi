package com.testproject.transformers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseDefinitionTransformer;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.ResponseDefinition;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class DynamicTransformer extends ResponseDefinitionTransformer {

    @Override
    public ResponseDefinition transform(Request request, ResponseDefinition responseDefinition, FileSource files, Parameters parameters) {
        String path = request.getUrl();
        String dynamicPath = path.substring(path.lastIndexOf("/")+1);     // Pull out the dynamic part
        String transformedJson = getResponseJson("person_response", dynamicPath); // Render the JSON string applicable
        return new ResponseDefinitionBuilder()
                .withHeader("Content-Type", "application/json")
                .withStatus(200)
                .withBody(transformedJson)
                .build();
    }

    @Override
    public String getName() {
        return "dynamic-transformer";
    }


    protected String getResponseJson(String name) {
        ObjectMapper mapper = new ObjectMapper();
        File resourceFile = new File("src/test/resources/responses/" + name + ".json");
        Object jsonObject = null;

        try {
            jsonObject = mapper.readValue(resourceFile, Object.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return asJsonString(jsonObject);
    }

    protected String getResponseJson(String name, String value) {
        ObjectMapper mapper = new ObjectMapper();
        JsonParser parser = new JsonParser();
        Object object = null;
        try {
            FileReader resourceFile = new FileReader("src/test/resources/responses/" + name + ".json");
            object = parser.parse(resourceFile);
            JsonObject jsonObject = (JsonObject) object;
            jsonObject = jsonObject.getAsJsonObject(value);
            object = mapper.readValue(jsonObject.toString(), Object.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return asJsonString(object);
    }


    protected String asJsonString(final Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}