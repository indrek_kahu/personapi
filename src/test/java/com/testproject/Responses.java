package com.testproject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.testproject.Utils.Constants;
import com.testproject.responses.ErrorResponse;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class Responses {

    private static final Logger LOG = LoggerFactory.getLogger(Responses.class);
    private static ObjectMapper objectMapper = new ObjectMapper();

    public static ApiTestResponse doGet(String url, Map<String, Object> params, Class outputClass) {
        Response response = given().port(Constants.PORT).params(params).accept(ContentType.JSON).when().get(url).then().contentType(ContentType.JSON).extract().response();
        try {
            Object responseObject;
            if (response.getStatusCode() != 200) {
                responseObject = objectMapper.readValue(response.getBody().print(), ErrorResponse.class);
            } else {
                responseObject = objectMapper.readValue(response.getBody().print(), outputClass);
            }

            return new ApiTestResponse(response.getStatusCode(), responseObject);
        } catch (IOException e) {
            LOG.error("Failed to parse response to object cause: " + e.getMessage());
        }
        return new ApiTestResponse(500, "Something went wrong.");
    }

    public static ApiTestResponse doGet(String url, Class outputClass) {
        Response response = given().port(Constants.PORT).accept(ContentType.JSON).when().get(url).then().extract().response();
        try {
            Object responseObject;
            if (response.getStatusCode() != 200) {
                responseObject = objectMapper.readValue(response.getBody().print(), ErrorResponse.class);
            } else {
                responseObject = objectMapper.readValue(response.getBody().print(), outputClass);
            }

            return new ApiTestResponse(response.getStatusCode(), responseObject);
        } catch (IOException e) {
            LOG.error("Failed to parse response to object cause: " + e.getMessage());
        }
        return new ApiTestResponse(500, "Something went wrong.");
    }
}
