package com.testproject.model.requests;

import java.util.Date;

public class PersonRequest {
    String id;
    String personalCode;
    String firstName;
    String lastName;
    Date birthday;
    Date deathDay;
    String gender;

    public String getId() {
        return id;
    }

    public String getPersonalCode() {
        return personalCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public Date getDeathDay() {
        return deathDay;
    }

    public String getGender() {
        return gender;
    }
}
