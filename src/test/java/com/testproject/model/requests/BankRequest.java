package com.testproject.model.requests;

public class BankRequest {

    String accountNumber;
    String currency;
    String bankName;

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getCurrency() {
        return currency;
    }

    public String getBankName() {
        return bankName;
    }
}
